var express = require('express');
var expressSession = require('express-session');
var passport = require('passport');
var app = express();
var http = require('http');
var server = http.Server(app);
var bodyParser = require('body-parser');
var morgan = require('morgan');
var path = require('path');
var mongoose = require('mongoose');
var errorhandler = require('errorhandler');
var port = process.env.PORT || 8080;
require('./configuration/MongoConfiguration');

var UserRouter = require('./routers/UserRouter');
var CompanyRouter = require('./routers/CompanyRouter');
var QuotaRouter = require('./routers/QuotaRouter');

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE');
  res.header('Access-Control-Expose-Headers', 'Content-Length');
  res.header('Access-Control-Allow-Headers', 'Accept, Authorization, Content-Type, X-Requested-With, Range, api_key');
  res.header('Access-Control-Max-Age', 60 * 60 * 24);
  if (req.method === 'OPTIONS') {
    return res.sendStatus(200);
  } else {
    return next();
  }
});

app.set('port', port);
app.use(expressSession({
  resave: false,
  saveUninitialized: false,
  secret: "secret"
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(bodyParser.urlencoded({
  limit: '100mb',
  extended: true
}));
app.use(bodyParser.json({
  limit: '100mb'
}));

require('./configuration/PassportConfiguration')

app.use('/api', UserRouter);
app.use('/api', CompanyRouter);
app.use('/api', QuotaRouter);


if ('development' === app.get('env')) {
  app.use(errorhandler());
}


var server = http.createServer(app);
server.listen(app.get('port'), function() {
  return console.log('Server jalan pada port ' + app.get('port'));
});

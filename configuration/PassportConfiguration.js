const User = require('../models/User');
var jwt = require('jsonwebtoken');
var passport = require('passport');
var PassportHttpBearerStrategy = require('passport-http-bearer').Strategy;

passport.use(new PassportHttpBearerStrategy(
  function(token, cb) {
    jwt.verify(token, "secret", function(err, decoded) {
      if (err) {
        return cb(null, false);
      } else {
        return cb(null, decoded);
      }
    });
  }));

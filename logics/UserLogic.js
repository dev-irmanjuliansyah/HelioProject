const User = require('../models/User');

var userLogic = {
  findUserForLogin: function(params, callback) {
    User.findOne({
      email: params.email
    })
      .populate({
        path: 'company_id',
        model: 'Company'
      })
      .exec(function(err, user) {
        if (err) {
          return callback(err);
        }
        if (user == null) {
          return callback({
            authenticate: false
          });
        }

        if (user.password == params.password) {
          return callback({
            authenticate: true,
            company_id: user.company_id._id
          });
        } else {
          return callback({
            authenticate: false
          });
        }
      });
  },
  findAllUser: function(company_id, callback) {
    User.find({
      company_id: company_id
    })
      .populate({
        path: 'company_id',
        model: 'Company'
      })
      .exec(function(err, users) {
        if (err) {
          return callback(err);
        }

        callback(users);
      });
  },

  saveUser: function(params, company_id, quota_id, callback) {
    var user = new User({
      fullname: params.fullname,
      email: params.email,
      phone_number: params.phone_number,
      password: params.password,
      role: params.role,
      status: params.status,
      company_id: company_id,
      quota_id: quota_id
    });

    user.save(function(err, userS) {
      if (err) {
        return callback(err);
      }
      callback(userS);
    });
  }
};

module.exports = userLogic;
